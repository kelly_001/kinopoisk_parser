<?php
session_start();
$return = [];
$data = [];
$error = false;
if(isset($_POST['date']))
{
	$error = false;
	$dateTo = htmlentities($_POST['date']);
	$pdo = new PDO('mysql:host=localhost;dbname=parser;charset=utf8', "parser", "parser");
	$stmt = $pdo->prepare("SELECT * FROM films WHERE year <= :year LIMIT 10"); 
	$stmt->execute([':year'=>$dateTo]);
	$arFilms = $stmt->fetchAll(PDO::FETCH_ASSOC);
	  /*while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
	  // $row - ассоциативный массив значений, ключи - названия столбцов
	    $arFilms[] = $row;
	  }*/
	$pdo = null;
	$_SESSION['films'] = $arFilms;
	$_SESSION['year'] = $dateTo;

    $data = ($error) ? array('error' => 'There was an error uploading your files') : array('data' => $arFilms);
}

echo(json_encode([$data]));




?>