<?php
include_once('simple_html_dom.php');
$return = [];
$data = [];

// получаем курлом данные с https://www.kinopoisk.ru/top/
$ch = curl_init("https://www.kinopoisk.ru/top/");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
//curl_setopt($ch, CURLOPT_HEADER, true);
$content = iconv('CP1251', 'UTF-8', curl_exec($ch));
curl_close($ch);


if(!empty($content))
{
	$error = false;

	//var_dump($content);
	$file = str_get_html($content);
	//connect to mysql
	$pdo = new PDO('mysql:host=localhost;dbname=parser;charset=utf8', "parser", "parser");
	$stmt = $pdo->prepare("INSERT INTO films (name, original_name, rating, position, year, people_voted) VALUES (?, ?,?,?,?,?)");

	$arRows = $file->find('tr[id^=top250_place_]');
	$countRows = count($arRows);
	$i = 0;
	$arRowVal = [];
	for ($i; $i<$countRows; $i++) {
		// парсим значения из хтмл
		$rating = floatval($arRows[$i]->children(2)->find('a',0)->innertext());
		$votes = str_replace(["(",")","&nbsp;"],"",$arRows[$i]->children(2)->find('span',0)->innertext());
		$name = $arRows[$i]->children(1)->find('a',0)->innertext();
		$year = str_replace(["(",")"],"",strstr($name, "("));
		$name = preg_replace("/\(.+\)/", "", $name);
		$original_name = "";
		if (!is_null($arRows[$i]->children(1)->find('span',0))) {
			$original_name = $arRows[$i]->children(1)->find('span',0)->innertext();
		}
		$position = $i + 1;
		//$position = str_replace(".","",$arRows[$i]->children(0)->innertext());
		// var_dump(str_replace(" ","",$arRows[$i]->children(2)->find('span',0)->innertext()));

		// подставляем значения в мускл запрос и выполняем его
		$stmt->bindParam(1, $name);
		$stmt->bindParam(2, $original_name);
		$stmt->bindParam(3, $rating);
		$stmt->bindParam(4, $position);
		$stmt->bindParam(5, $year);
		$stmt->bindParam(6, $votes);

		$stmt->execute();
		//var_dump($stmt->errorInfo());
	}
    $dbh = null;
} else {
	$error = true;
   
}


?>