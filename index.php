<?php
session_start();
  if (!isset($_SESSION['films'])) {
    $pdo = new PDO('mysql:host=localhost;dbname=parser;charset=utf8', "parser", "parser");
    $res = $pdo->query("SELECT * FROM films LIMIT 10"); 
    $arFilms = [];
    while ($row = $res->fetch(PDO::FETCH_ASSOC)){
    // $row - ассоциативный массив значений, ключи - названия столбцов
      $arFilms[] = $row;
    }
    $pdo = null;
    $_SESSION['films'] = $arFilms;
  } else {
  $arFilms = $_SESSION['films'];
}
// var_dump ($_SESSION);
  
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Kinopoisk Top - 10</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="index.css" rel="stylesheet">
  </head>

  <body>

    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <a class="navbar-brand" href="#"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="container">

      <div>
        <form method="post" id="dateForm">
          <div class="form-row align-items-center">
            <div class="col-auto">
              <label class="sr-only" for="inlineFormInput">Date</label>
              <input type="text" class="form-control mb-2" id="inlineFormInput" placeholder="Год" name="date" 
              <? if(isset($_SESSION['year'])) echo ("value = '" . $_SESSION['year'] . "'" )?>>
            </div>
             <div class="col-auto">
              <button type="submit" class="btn btn-primary mb-2">Показать</button>
            </div>
        </div>
        </form>
      </div>

      <h1>Топ - 10 рейтинга Кинопоиска </h1>

      <div class="table">
        <table class="table" id="topTable">
          <thead>
            <tr>
              <th scope="col">Позиция</th>
              <th scope="col">Название (год)</th>
              <th scope="col">Рейтинг</th>
            </tr>
          </thead>
          <tbody>
            <? foreach( $arFilms as $film):?>
              <tr>
                <td scope="row"><?=$film['position']?></td>
                <td><?=$film['name']?>(<?=$film['year']?>)</td>
                <td><?=$film['rating']?></td>
              </tr>
            <?endforeach;?>
          </tbody>
        </table>
      </div>
      <? // var_dump($arFilms) ?>
    </main><!-- /.container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script
    src="http://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
  </body>
  <script>
  $( document ).ready(function() {

    $("#dateForm").submit(function(event){
      event.stopPropagation(); // Stop stuff happening
      event.preventDefault(); // Totally stop stuff happening
      var $form = $(this);

     $.ajax({
        type: $form.attr('method'),
        url: "script",
        data:  $form.serialize(),
        dataType: 'json',
        success: function (data) { 
          var datas = data[0]['data'];
          console.log(datas);
          $html = '';
          for(var i=0; i<datas.length; i++) {
            $html += "<tr><td scope='row'>" + datas[i]['position'] + "</td><td>" + datas[i]['name'] + " (" + datas[i]['year'] +") </td><td>" + datas[i]['rating'] +"</td></tr>";
          } 
          $('#topTable tbody').html($html);
          
        }
    });
    })
  });
  </script>
</html>